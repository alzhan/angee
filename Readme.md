# Description
[WPDownloader] WPDownloader is a program provides web page and it's resources download
<author>Alzhan Turlybekov</author>

#Compilation
cmake -DCMAKE_BUILD_TYPE=<BUILD_TYPE> <cmakelists.txt_dir>
make

Example:

mkdir build
mkdir release
cd build/release
cmake -DCMAKE_BUILD_TYPE=Release ../..
make

# Dependencies

libcurl, libboost-program-options, libboost-filesystem, libcrypto++

# Install needed dependencies

sudo apt install libcurl4-gnutls-dev
sudo apt-get install libboost-program-options-dev
sudo apt-get install libboost-filesystem-dev
sudo apt-get install libcrypto++-dev

All other libraries including gtest should be downloaded by cmake

# Run

Binary: bin/Release/WPDownloader

Allowed options:
  --help                                     produce help message
  --webpage arg (=https://www.meetangee.com)
                                             webpage from which resources will be 
                                             downloaded
  --threadNumber arg (=4)                    thread number
  --attributeKeyFilter arg (= href src style)
                                             attribute key filter
  --attributeValueFilter arg (= .jpg .png .ico .svg .js)
                                             attribute value filter
  --skipExistingFiles                        skip downloading of existing files

Run with default args: ./WPDownloader

Example:

Download www.google.com as html and resources in src and style attribute containing .jpg or .png:
./WPDownloader --webpage https://www.google.com --attributeKeyFilter src style --attributeValueFilter .jpg .png

# Tests

Warning: to ensure all tests will run properly, copy testData from test folder to the runtime directory

Tests is written using gtest framework.
Binary: bin/Release/test_WPDownloader

Run: ./test_WPDownloader

# Directory structure
bin/	    		             -- this directory contains program binary
docs/				               -- documentation
ext/				               -- library sources
libs/				               -- compiled libraries 
include/                   -- includes
src/				               -- sources
test/                      -- sources of test project
main.cpp                   -- Main application
CMakeLists.txt             -- CMakeLists.txt file used to create Makefile
loggerConfig.conf.template -- logger configuration file


# Logger configuration
loggerConfig.conf.template place to the runtime directory and rename to loggerConfig.conf
Configuration documentation: https://github.com/muflihun/easyloggingpp
