#include "WPDownloader.h"

#include "easylogging++.h"

#include <boost/program_options.hpp>

#include <iostream>
#include <limits>

INITIALIZE_EASYLOGGINGPP

using namespace at;
using namespace el;
using namespace boost::program_options;
using namespace std;

namespace
{
static const char* HELP = "help";
static const char* WEB_PAGE = "webpage";
static const char* THREAD_NUMBER = "threadNumber";
static const char* ATTRIBUTE_KEY_FILTER = "attributeKeyFilter";
static const char* ATTRIBUTE_VALUE_FILTER = "attributeValueFilter";
static const char* SKIP_EXISTING_FILES = "skipExistingFiles";

//const map<string, std::unique_ptr<HashTransformation>> ALGORITHMS = { {"Adler32", new Adler32()}, {"CRC32", new CRC32()} };
}

namespace std
{

std::ostream& operator<<(std::ostream& out, const std::vector<std::string>& s) 
{
    for (const auto& value : s)
    {
        out << " " << value;
    }
    return out;
}

} // namespace std

int parseCommandLine(int argc, char** argv, variables_map& vm)
{
    const vector<string> keys{"href", "src", "style"};
    const vector<string> searchFilter{".jpg", ".png", ".ico", ".svg", ".js"};

    // Declare the supported command line options.
    options_description desc("Allowed options");
    desc.add_options()
        (HELP, "produce help message")
        (WEB_PAGE, value<string>()->default_value("https://www.meetangee.com"), "webpage from which resources will be downloaded")
        (THREAD_NUMBER, value<int>()->default_value(4), "sets thread number")
        (ATTRIBUTE_KEY_FILTER, value<vector<string>>()->multitoken()->default_value(keys), "attribute key filter")
        (ATTRIBUTE_VALUE_FILTER, value<vector<string>>()->multitoken()->default_value(searchFilter), "attribute value filter")
        (SKIP_EXISTING_FILES, "skip downloading of existing files");

    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    if (vm.count(HELP))
    {
        cout << desc << "\n";
        return 1;
    }

    return 0;
}

int main(int argc, char** argv)
{
    START_EASYLOGGINGPP(argc, argv);
    Configurations conf("loggerConfig.conf");
    Loggers::reconfigureLogger("default", conf);
    Loggers::reconfigureAllLoggers(conf);

    variables_map vm;
    int parseResult = parseCommandLine(argc, argv, vm);
    if (parseResult != 0)
    {
        return parseResult;
    }

    WPDownloader wpd;

    string data;
    if (!wpd.downloadWebPage(vm[WEB_PAGE].as<string>(), data))
    {
        return 1;
    }

    const auto& attributeKeys = vm[ATTRIBUTE_KEY_FILTER].as<vector<string>>();
    const auto& attributeValueFilter = vm[ATTRIBUTE_VALUE_FILTER].as<vector<string>>();

    HTMLSearchResult resources;
    if (!wpd.getResourcesFromHTML(data,
                                  { attributeKeys.cbegin(), attributeKeys.cend() },
                                  { attributeValueFilter.cbegin(), attributeValueFilter.cend() },
                                  resources))
    {
        return 1;
    }

    bool rewriteExistingFiles = !vm.count(SKIP_EXISTING_FILES);
    const auto&& fileNames = wpd.downloadResources(vm[THREAD_NUMBER].as<int>(), rewriteExistingFiles, resources);

    vector<string> hashes;
    vector<string> fileNamesVec{fileNames.begin(), fileNames.end()};
    if (!wpd.createHash(fileNamesVec, hashes))
    {
        return 1;
    }

    wpd.printFilesInfo(fileNamesVec, hashes);

    return 0;
}
