#include "URLDownloader.h"

#include <string>
#include <vector>

#include <boost/filesystem.hpp>

#include "gtest/gtest.h"

using namespace at;
using namespace boost::filesystem;
using namespace std;

TEST(URLDownloaderTest, Download)
{
	const vector<string> urls = {"https://www.meetangee.com", "https://www.seznam.cz"};
	URLDownloader urlDownloader;

	for (const auto& url : urls)
	{
		EXPECT_TRUE(urlDownloader.download(url));
	}
}

TEST(URLDownloaderTest, DownloadAndSaveToFile)
{
	const vector<string> urls = {"https://www.meetangee.com", "https://www.seznam.cz"};
	const char* dir = "testResources/";

	URLDownloader urlDownloader;

	create_directory(dir);

	int i = 0;
	for (const auto& url : urls)
	{
		EXPECT_TRUE(urlDownloader.download(url));
		EXPECT_TRUE(urlDownloader.saveToFile(dir + to_string(i)));
		++i;
	}
}