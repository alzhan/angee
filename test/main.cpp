#include "easylogging++.h"

#include "gtest/gtest.h"

INITIALIZE_EASYLOGGINGPP

using namespace el;

int main(int argc, char **argv)
{
    START_EASYLOGGINGPP(argc, argv);
    Configurations conf("testLoggerConfig.conf");
    Loggers::reconfigureLogger("default", conf);
    Loggers::reconfigureAllLoggers(conf);

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
