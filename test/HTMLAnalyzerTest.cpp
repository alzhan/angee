#include "HTMLAnalyzer.h"

#include <string>
#include <vector>
#include <fstream>

#include "gtest/gtest.h"

using namespace at;
using namespace std;

TEST(HTMLAnalyzerTest, Init)
{
    HTMLAnalyzer analyzer;
    EXPECT_TRUE(analyzer.init());
}

TEST(HTMLAnalyzerTest, ParseSmallHTML)
{
    HTMLAnalyzer analyzer;
    EXPECT_TRUE(analyzer.init());

    const string dir = "testData/";
    const int dataSize = 4;
    
    for (int i = 0; i < dataSize; ++i)
    {
        string fileName = dir + "small" + to_string(i + 1) + ".html";
        ifstream in(fileName);
        stringstream buffer;
        buffer << in.rdbuf();

        EXPECT_TRUE(analyzer.parse(buffer.str())) << "Falied on: " << fileName;
    }
}
