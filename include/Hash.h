#ifndef AT_HASH_H
#define AT_HASH_H

#include <cryptopp/cryptlib.h>

#include <set>
#include <vector>
#include <string>

namespace at
{

/*! \brief This class provides hash.
 */
class Hash
{
public:

    /*!
     * This method creates hash for given \a str string
     * \param  hashFilter hash filter which will be used
     * \param  str string to hash
     * \return result hash as a string
     */
    std::string createHash(CryptoPP::HashTransformation& hashFilter, const std::string& str);

    /*!
     * This method creates hash for given \a strs
     * \param  hashFilter hash filter which will be used
     * \param  strs strings to hash
     * \return result vector of hashes
     */
     std::vector<std::string> createHash(CryptoPP::HashTransformation& hashFilter, const std::vector<std::string>& strs);
};

} // namespace at

#endif // !AT_HASH_H
