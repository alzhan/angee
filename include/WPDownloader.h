#ifndef AT_WPDOWNLOADER_H
#define AT_WPDOWNLOADER_H

#include "HTMLAnalyzer.h"

namespace at
{

/*! \brief This class represents main app class providing methods to download a webpage and 
           it's resources, creating hash and printing out the result.
 */
class WPDownloader
{
public:
    /*! Download a webpage located at \a webpage using the URLDownloader class.
     *  Also function checks if url is valid by the URLAnalyzer class.
     *  Saves the downloaded webpage to a file.
     *  @param[in]  webpage webpage url
     *  @param[out] data webpage data as a string
     *  @return     true if download succeed, otherwise false
     */
    bool downloadWebPage(const std::string& webpage, std::string& data);

    /*! Get the all resources from \given html
     *  @param[in]  html to be analyzed
     *  @param[in]  attributeKeys attribute keys
     *  @param[in]  attributeValueFilter attribute value filter
     *  @param[out] resources found resources
     *  @return     true if succeed, otherwise false
     */
    bool getResourcesFromHTML(const std::string& html, const HTMLAttributeKeys& attributeKeys, 
                              const HTMLSearchFilter& attributeValueFilter,
                              HTMLSearchResult& resources);

    /*! Download all resources using the URLDownloader class and ThreadPool
     *  @param[in]  threadNumber number of thread to be used
     *  @param[in]  rewriteExistingFiles if true existing files will be rewritten
     *  @param[in]  attributeValueFilter attribute value filter
     *  @param[out] resources to be downloaded
     *  @return     std::set<std::string> set of downloaded file names
     */
    std::set<std::string> downloadResources(int threadNumber, bool rewriteExistingFiles, const HTMLSearchResult& resources);

    /*! Creates hash of given files
     *  @param[in]  fileNames files to be hashed
     *  @param[out] hashes hash vector
     *  @return     true if succeed, otherwise false
     */
    bool createHash(const std::vector<std::string>& fileNames, std::vector<std::string>& hashes);

    /*! Prints out the file names and their hashes.
     *  Highlights the smallest (green color) and the biggest (red color) file (in size)
     *  @param[in]  fileNames file names to be printed
     *  @param[in]  hashes corresponding ot \a fileNames
     */
    void printFilesInfo(const std::vector<std::string>& fileNames, const std::vector<std::string>& hashes);
};

} // namespace at

#endif // !AT_WPDOWNLOADER_H