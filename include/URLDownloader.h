#ifndef AT_URLDOWNLOADER_H
#define AT_URLDOWNLOADER_H

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

#include <string>
#include <sstream>

namespace at
{

/*! \brief This class wraps curlpp library.
 *         It provides download a source by URL.
 */
class URLDownloader
{
public:
    /*! Download a source located at \a url
     *  @param url source url
     *  @return true if download succeed, otherwise false
     */
    bool download(const std::string& url);

    /*! Saves downloaded source to file named \a filename
     *  @param filename name of the file
     *  @return true if save succeed, otherwise false
     */
    bool saveToFile(const std::string& fileName) const;

    /*! Returns downloaded source as a string
     *  @return source as a string
     */
    inline std::string getData() const { return buffer_.str(); }

private:
    curlpp::Easy easyCurl_;
    std::ostringstream buffer_;
};

} // namespace at

#endif // !AT_URLDOWNLOADER__H
