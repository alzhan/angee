#ifndef AT_HTML_ANALYZER_H
#define AT_HTML_ANALYZER_H

#include <myhtml/api.h>

#include <set>
#include <vector>
#include <string>

namespace at
{
using HTMLSearchResult  = std::set<std::string>;
using HTMLSearchFilter  = std::set<std::string>;
using HTMLAttributeKeys = std::set<std::string>;

/*! \brief This class wraps myhtml library.
 *         It provides the methods to analyze a given HTML.
 */
class HTMLAnalyzer
{
public:
    HTMLAnalyzer();
    ~HTMLAnalyzer();

    /*! Initialize the parser
     *  \warning Should be called before calling \a parse method
     *  @param threadNumber number of threads, default = 1
     */
    bool init(size_t threadNumber = 1);

    /*! Parse given \a html
     *  @param html HTML to be parsed
     */
    bool parse(const std::string& html);

    /*! Find all attribute values of given \a keys in the parsed html which satisfies \a filter
     *  @param   keys              attribute keys where the search will be performed
     *  @param   filter            attribute values filter
     *  @return  HTMLSearchResult  set of all found attribute values
     */
    HTMLSearchResult FindAttributeValues(const HTMLAttributeKeys& keys,
                                         const HTMLSearchFilter&  filter) const;

    /*! Find all attribute value in the parsed html in \a key containing given \a value substring
     *  @param  key           attribute key where the search will be performed
     *  @param  value         attribute value substring
     *  @return HTMLSearchResult  set of all found attribute values
     */
    HTMLSearchResult FindAttributeValues(const std::string& key, const std::string& value) const;

private:
    myhtml_t* myhtml_;
    myhtml_tree_t* tree_;
};

} // namespace at

#endif // !AT_HTML_ANALYZER_H
