#ifndef AT_URL_ANALYZER_H
#define AT_URL_ANALYZER_H

#include <set>
#include <string>
#include <tuple>

namespace at
{

/*! \brief This class provide methods to analyze given URL.
 */
class URLAnalyzer
{
public:
    using URLs = std::set<std::string>;
    using ValidationResult = std::tuple<bool, bool, std::string>;
    using FileNameAndExtensionPair = std::pair<std::string, std::string>; ///< <FileName, Extension>

    /*!
     * Validates \a URL eventually parses the text and returns valid URL if exists
     * @param url which will be validated
     * @return ValidationResult if success returns <true, changed, correct url> otherwise <false, changed, empty string>
     *         where changed determines whether url was obtained from text
     */
    ValidationResult validate(const std::string& url) const;

    /*!
     * Validates \a URLs and returns only valid URLs
     * @param[in, out] urls returns only valid urls
     */
    void validate(URLs& urls) const;

    /*!
     * Creates directory name and file name from \a URL
     * \warning won't validate the url
     * @return FileNameAndExtensionPair <FileName, Extension> pair
     */
    FileNameAndExtensionPair createFileName(const std::string& url) const;
};

} // namespace at

#endif // !AT_URL_ANALYZER_H
