
set(SOURCE_LIB WPD)

set(SRC_LIST WPDownloader.cpp
             URLDownloader.cpp
             HTMLAnalyzer.cpp
             URLAnalyzer.cpp
             Hash.cpp)

set(INCLUDE_DIR ${CMAKE_SOURCE_DIR}/include/)
include_directories(${INCLUDE_DIR})

set(HDRS_LIST ${INCLUDE_DIR}/WPDownloader.h
              ${INCLUDE_DIR}/URLDownloader.h 
              ${INCLUDE_DIR}/HTMLAnalyzer.h
              ${INCLUDE_DIR}/URLAnalyzer.h
              ${INCLUDE_DIR}/Hash.h)

add_library(${SOURCE_LIB} ${SRC_LIST} ${HDRS_LIST})
set_target_properties(${SOURCE_LIB} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/lib")
######

###LIBS###

#easylogging++#
add_library(EasyLogging "${CMAKE_SOURCE_DIR}/ext/easylogging++/src/easylogging++.cpp")
set_target_properties(EasyLogging PROPERTIES ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/lib")

if(CMAKE_BUILD_TYPE STREQUAL "Release")
    message("Disable debug logs")
    add_definitions(-DELPP_DISABLE_DEBUG_LOGS)
endif()
######

#CURL#
find_package(CURL REQUIRED)

if(NOT CURL_FOUND)
    message(SEND_ERROR "Failed to find CURL")
    return()
else()
    include_directories(${CURL_INCLUDE_DIR})
endif()
######

#Boost program_options & filesystem#
find_package(Boost REQUIRED COMPONENTS program_options filesystem)

if (NOT Boost_FOUND)
    message(SEND_ERROR "Failed to find Boost program_options, filesystem")
    return()
else()
    include_directories(${Boost_INCLUDE_DIRS})
endif()
#####

#CURLpp v0.8.1#
# Enable ExternalProject CMake module
include(ExternalProject)

# Download and install CURLpp
ExternalProject_Add(curlpp
                    URL https://github.com/jpbarrette/curlpp/archive/v0.8.1.zip
                    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/curlpp
                    INSTALL_COMMAND "")

# Get curlpp source and binary directories from CMake project
ExternalProject_Get_Property(curlpp source_dir binary_dir)

# Set parent include path variable
set(CURLPP_INCLUDE_DIR ${source_dir}/include PARENT_SCOPE)

# Set lib path variable
set(CURLPP_LIB_DIR ${binary_dir}/libcurlpp.a)

include_directories(${source_dir}/include)

add_dependencies(${SOURCE_LIB} curlpp)
######

#MyHTML v4.0.4#
# Download and install MyHTML
ExternalProject_Add(myhtml
                    URL https://github.com/lexborisov/myhtml/archive/v4.0.4.zip
                    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/myhtml
                    INSTALL_COMMAND "")

# Get myhtml source and binary directories from CMake project
ExternalProject_Get_Property(myhtml source_dir binary_dir)

# Set parent include path variable
set(MYHTML_INCLUDE_DIR ${source_dir}/include PARENT_SCOPE)

# Set lib path variable
set(MYHTML_LIB_DIR ${binary_dir}/libmyhtml_static.a)

include_directories(${source_dir}/include)

add_dependencies(${SOURCE_LIB} myhtml)
######

target_link_libraries(${SOURCE_LIB} EasyLogging ${CURL_LIBRARIES} ${CURLPP_LIB_DIR} ${MYHTML_LIB_DIR} -lpthread
                      ${Boost_PROGRAM_OPTIONS_LIBRARY} ${Boost_FILESYSTEM_LIBRARY} ${Boost_SYSTEM_LIBRARY} -lcryptopp)

##########
