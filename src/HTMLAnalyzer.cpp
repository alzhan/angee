#include "HTMLAnalyzer.h"

#include "easylogging++.h"

using namespace at;
using namespace std;

HTMLAnalyzer::HTMLAnalyzer() :
    myhtml_(nullptr),
    tree_(nullptr)
{
}

HTMLAnalyzer::~HTMLAnalyzer()
{
    myhtml_tree_destroy(tree_);
    myhtml_destroy(myhtml_);
}

bool HTMLAnalyzer::init(size_t threadNumber)
{
    myhtml_ = myhtml_create();
    if (myhtml_ == nullptr)
    {
        LOG(ERROR) << "Unable to create myhtml object";
        return false;
    }

    mystatus_t status = myhtml_init(myhtml_, MyHTML_OPTIONS_DEFAULT, threadNumber, 0);
    if (status != MyHTML_STATUS_OK)
    {
        LOG(ERROR) << "Unable to initialize myhtml, error code: " << status;
        return false;
    }

    tree_ = myhtml_tree_create();
    if (tree_ == nullptr)
    {
        LOG(ERROR) << "Unable to create myhtml tree object";
        return false;
    }

    status = myhtml_tree_init(tree_, myhtml_);
    if (status != MyHTML_STATUS_OK)
    {
        LOG(ERROR) << "Unable to initialize myhtml tree, error code: " << status;
        return false;
    }

    LOG(INFO) << "HTMLAnalyzer successfully initialized";
    return true;
}

bool HTMLAnalyzer::parse(const string& html)
{
    if (myhtml_ == nullptr || tree_ == nullptr)
    {
        LOG(ERROR) << "HTMLAnalyzer was not initialized" ;
        return false;
    }

    if (html.empty())
    {
        LOG(WARNING) << "HTML is empty" ;
        return false;   
    }

    mystatus_t status = myhtml_parse(tree_, MyENCODING_UTF_8, html.c_str(), html.size());
    if (status != MyHTML_STATUS_OK)
    {
        LOG(ERROR) << "Parsing error, code: " << status;
        return false;
    }

    LOG(INFO) << "HTML successfully parsed";
    return true;
}

HTMLSearchResult HTMLAnalyzer::FindAttributeValues(const HTMLAttributeKeys& keys, const HTMLSearchFilter& filter) const
{
    HTMLSearchResult result;
    for (const auto& key : keys)
    {
        for (const auto& valueSubstring : filter)
        {
            auto res = FindAttributeValues(key, valueSubstring);

            if (!res.empty())
            {
                result.insert(res.cbegin(), res.cend());
            }
        }
    }
    return result;
}

HTMLSearchResult HTMLAnalyzer::FindAttributeValues(const string& key, const string& value) const
{
    HTMLSearchResult result;

    if (myhtml_ == nullptr || tree_ == nullptr)
    {
        LOG(ERROR) << "HTMLAnalyzer was not initialized" ;
        return result;
    }

    mystatus_t status;
    myhtml_collection_t* collection = myhtml_get_nodes_by_attribute_value_contain(tree_, nullptr,
                                                                                  nullptr, true,
                                                                                  key.c_str(), key.size(),
                                                                                  value.c_str(), value.size(),
                                                                                  &status);

    if (collection == nullptr)
    {
        LOG(DEBUG) << "Nothing found, status: " << status;
        return result;
    }

    LOG(DEBUG) << "(key, value)=(" << key << ", " << value << ")" << ". Collection size: " << collection->length;

    for (size_t i = 0; i < collection->length; ++i)
    {
        myhtml_tree_attr_t* attributes = myhtml_attribute_by_key(collection->list[i], key.c_str(), key.size());
        if (attributes != nullptr)
        {
            const char* attrValue = myhtml_attribute_value(attributes, nullptr);
            if (attrValue != nullptr)
            {
                result.insert(attrValue);
                LOG(DEBUG) << "Added attribute value: " << attrValue;
            }
        }
        else
        {
            LOG(WARNING) << "Attributes result is null";
        }
    }

    myhtml_collection_destroy(collection);
    return result;
}
