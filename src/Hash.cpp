#include "Hash.h"

#include "easylogging++.h"

#include <cryptopp/adler32.h>
#include <cryptopp/files.h>
#include <cryptopp/filters.h>
#include <cryptopp/hex.h>

using namespace at;
using namespace CryptoPP;
using namespace std;

string Hash::createHash(HashTransformation& hashFilter, const string& str)
{
    std::string hash;
    CryptoPP::StringSource(str.c_str(), true,
                           new HashFilter(hashFilter, new HexEncoder(new StringSink(hash))));

    LOG(DEBUG) << "Created hash: " << hash << ", for: " << str;
    return hash;
}

vector<string> Hash::createHash(HashTransformation& hashFilter, const vector<string>& strs)
{
    vector<string> result;
    for (const auto& str : strs)
    {
        result.push_back(createHash(hashFilter, str));
    }
    return result;
}
