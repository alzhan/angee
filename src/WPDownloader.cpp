#include "WPDownloader.h"

#include "URLDownloader.h"
#include "HTMLAnalyzer.h"
#include "URLAnalyzer.h"
#include "Hash.h"

#include "easylogging++.h"
#include "ThreadPool.h"

#include <boost/filesystem.hpp>

#include <cryptopp/adler32.h>
#include <cryptopp/crc.h>
#include <cryptopp/sha.h>

#include <vector>
#include <set>
#include <iostream>

using namespace at;
using namespace CryptoPP;
using namespace boost::filesystem;
using namespace std;

namespace
{
static const char* RESOURCE_DIR = "resources/";
}

bool WPDownloader::downloadWebPage(const string& webpage, string& data)
{
    URLAnalyzer urlAnalyzer;
    const auto&& result = urlAnalyzer.validate(webpage);

    if (!get<0>(result) || get<1>(result))
    {
        cout << "Incorrect webpage: " << webpage << "\n";
        return false;
    }

    cout << "Downloading " << webpage << endl;
    URLDownloader downloader;
    if (!downloader.download(webpage))
    {
        return false;
    }

    string fileName = urlAnalyzer.createFileName(webpage).first;
    if (!fileName.empty())
    {
        downloader.saveToFile(fileName);
    }

    data = downloader.getData();
    LOG(DEBUG) << data;

    return true;
}

bool WPDownloader::getResourcesFromHTML(const string& html, const HTMLAttributeKeys& attributeKeys,
                                        const HTMLSearchFilter& attributeValueFilter,
                                        HTMLSearchResult& resources)
{
    HTMLAnalyzer htmlAnalyzer;
    if (!htmlAnalyzer.init())
    {
        return false;
    }

    if (!htmlAnalyzer.parse(html))
    {
        return false;
    }

    resources = std::move(htmlAnalyzer.FindAttributeValues(attributeKeys, attributeValueFilter));

    // Remove invalid resource urls
    URLAnalyzer urlAnalyzer;
    urlAnalyzer.validate(resources);

    if (resources.empty())
    {
        LOG(INFO) << "No valid resources were found";
        return false;
    }

    return true;
}

set<string> WPDownloader::downloadResources(int threadNumber, bool rewriteExistingFiles, const HTMLSearchResult& resources)
{
    set<string> fileNames;
    URLAnalyzer urlAnalyzer;

    cout << "Downloading resources..." << endl;
    {
        ThreadPool pool(threadNumber);
        LOG(INFO) << "Created thread pool with " << threadNumber << " threads";

        auto downloadFunc = [](const string& resource, const string& fileName) -> bool
        {
            URLDownloader urlDownloader;
            return urlDownloader.download(resource) && urlDownloader.saveToFile(fileName);
        };

        create_directory(RESOURCE_DIR);

        // Download all valid resources
        for (const auto& resource : resources)
        {
            const auto&& fileNameExt = urlAnalyzer.createFileName(resource);
            if (fileNameExt.first.empty())
            {
                LOG(WARNING) << "Empty fileNameExt";
                continue;
            }

            string dir = RESOURCE_DIR;
            if (!fileNameExt.second.empty())
            {
                 dir += fileNameExt.second + "/";
            }

            if (!exists(dir))
            {
                create_directory(dir);
            }

            string fileName = dir + fileNameExt.first;
            if (rewriteExistingFiles || !exists(fileName))
            {
                LOG(DEBUG) << "File: " << fileName << " will be downloaded";
                
                pool.enqueue(downloadFunc, resource, fileName);
                fileNames.insert(fileName);
            }
            else
            {
                LOG(DEBUG) << "File: " << " exists, skip downloading";
            }
        }
    } // block to join all threads (it is done in destructor of ThreadPool)

    return fileNames;
}

bool WPDownloader::createHash(const vector<string>& fileNames, vector<string>& hashes)
{
    Adler32 ht;

    Hash hash;
    hashes = std::move(hash.createHash(ht, fileNames));

    if (hashes.size() != fileNames.size())
    {
        LOG(ERROR) << "Hashes number differs from file number";
        return false;
    }

    return true;
}

void WPDownloader::printFilesInfo(const vector<string>& fileNames, const vector<string>& hashes)
{
    int maxSize = 0;
    int minSize = numeric_limits<int>::max();
    
    int maxFileSizeIndex = 0;
    int minFileSizeIndex = 0;

    for (unsigned int i = 0; i < hashes.size(); ++i)
    {
        int fileSize = file_size(fileNames[i]);
        if (maxSize < fileSize)
        {
            maxSize = fileSize;
            maxFileSizeIndex = i;
        }
        if (minSize > fileSize)
        {
            minSize = fileSize;
            minFileSizeIndex = i;
        }
    }

    for (unsigned int i = 0; i < hashes.size() && i < fileNames.size(); ++i)
    {
        const auto& fileName = fileNames[i];
        if (i == maxFileSizeIndex)
        {
            cout << "File: " << "\033[0;41m" << fileName  << "\033[0;0m" << " [" 
                 << file_size(fileName) << " B]\t Hash: " << hashes[i] << endl;
        }
        else if (i == minFileSizeIndex)
        {
            cout << "File: " << "\033[0;42m" << fileName << "\033[0;0m" << " [" 
                 << file_size(fileName) << " B]\t Hash: " << hashes[i] << endl;
        }
        else
        {
            cout << "File: " << fileName << " [" << file_size(fileName) << " B]\t Hash: " << hashes[i] << endl;
        }
    }
    cout << "Total files: \t" << fileNames.size() << endl;
}
