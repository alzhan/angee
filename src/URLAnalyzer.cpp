#include "URLAnalyzer.h"

#include "easylogging++.h"

#include <regex>

using namespace at;
using namespace std;

namespace
{
static const char* URL_REGEX = R"((http|ftp|https):\/\/([\w+?\.\w+])+([a-zA-Z0-9\~\!\@\#\$\%\^\&\*\-\=\+\\\/\?\.\:\;\,]*)?)";
}

URLAnalyzer::ValidationResult URLAnalyzer::validate(const string& url) const
{
    regex urlRegex{URL_REGEX};

    std::smatch match;
    if (regex_search(url, match, urlRegex) && !match.empty())
    {
        LOG(DEBUG) << "Matched str: " << match.str();
        return make_tuple(true, match.str() != url, match.str());
    }

    LOG(DEBUG) << "No match";
    return make_tuple(false, false, string());
}

void URLAnalyzer::validate(URLs& urls) const
{
    for (auto it = urls.begin(); it != urls.end(); /*Don't increment*/)
    {
        auto result = validate(*it);
        if (get<0>(result))
        {
            if (get<1>(result))
            {
                LOG(DEBUG) << "URL was matched and changed. Erase old value and insert new one";
                it = urls.erase(it);
                urls.insert(get<2>(result));
            }
            else
            {
                LOG(DEBUG) << "URL was matched and was not changed. Leave in the set";
                ++it;
            }
        }
        else
        {
            LOG(DEBUG) << "URL was not matched, erasing";
            it = urls.erase(it);
        }
    }
}

URLAnalyzer::FileNameAndExtensionPair URLAnalyzer::createFileName(const string& url) const
{
    FileNameAndExtensionPair result;

    size_t foundSlash = url.find_last_of('/');
    if (foundSlash == string::npos)
    {
        LOG(ERROR) << "Incorrect url: " << url;
        return result;
    }

    size_t foundQuestionMark = url.find_last_of("?#");
    foundQuestionMark = (foundQuestionMark == string::npos) ? url.size() : foundQuestionMark;

    result.first = url.substr(foundSlash + 1, foundQuestionMark - foundSlash - 1);

    size_t foundDot = result.first.find_last_of('.');
    if (foundDot == string::npos)
    {
        LOG(DEBUG) << "No extension: " << url;
        return result;
    }

    size_t extensionEnd = result.first.find_last_of("?#");
    extensionEnd = (extensionEnd == string::npos) ? result.first.size() : extensionEnd;

    result.second = result.first.substr(foundDot + 1, extensionEnd - foundDot - 1);

    LOG(DEBUG) << "fileName: " << result.first << ", ext: " << result.second << ", url: " << url;
    return result;
}
