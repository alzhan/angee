#include "URLDownloader.h"

#include "easylogging++.h"

#include <fstream>

using namespace at;
using namespace curlpp;
using namespace curlpp::options;
using namespace std;

bool URLDownloader::download(const string& url)
{
    easyCurl_.setOpt<Url>(url);
    easyCurl_.setOpt<SslVerifyPeer>(false);

    curlpp::options::WriteStream ws(&buffer_);
    easyCurl_.setOpt(ws);

    try
    {
        LOG(INFO) << "Downloading from: " << url;
        easyCurl_.perform();
    }
    catch (LogicError& e)
    {
        LOG(ERROR) << e.what();
        return false;
    }
    catch (RuntimeError& e)
    {
        LOG(ERROR) << e.what();
        return false;
    }

    LOG(INFO) << url << " downloaded" ;
    return true;
}

bool URLDownloader::saveToFile(const std::string& fileName) const
{
    ofstream out{fileName};

    if (out.is_open())
    {
        out << getData() << endl;
        out.close();
        LOG(DEBUG) << "Saved to file: " << fileName;
        return true;
    }

    LOG(DEBUG) << "Unable to save to file: " << fileName;
    return false;
}
